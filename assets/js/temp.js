var canvas = new fabric.Canvas("c");

fabric.loadSVGFromURL("./assets/m-ir.svg", function (objects, options) {
  // Create an empty array to hold the path objects
  console.log(objects);
  var paths = [];

  // Loop through the objects array and create a new path object for each path
  objects.forEach(function (obj) {
    if (obj instanceof fabric.Path) {
      // Create a new path object with the same properties as the loaded object
      var path = new fabric.Path(obj.path, {
        left: obj.left,
        top: obj.top,
        fill: "green",
        stroke: obj.stroke,
        strokeWidth: obj.strokeWidth,
        selectable: true,
        lockMovementX: true,
        lockMovementY: true,
        lockRotation: true,
        lockScalingX: true,
        lockScalingY: true,
        perPixelTargetFind: true,
      });
      path.set({ id: obj.id });
      path.set({ name: obj.name });

      // Add an event listener for mouse down
      // path.on("mouseover", function (event) {
      //   var point = new fabric.Point(event.e.clientX, event.e.clientY);
      //   if (path.containsPoint(point) && path.fill === 'green') {
      //     console.log("Clicked inside the path");
      //   } else {
      //     path.set('fill','red')
      //     console.log("Clicked outside the path",path.id);
      //   }

      // //   if (path.fill === "red") {
      // //     if (path.containsPoint({ x: x, y: y })) {
      // //       console.log("Path object clicked");
      // //     }
      // //   }

      //   // Do something here, such as change the fill color or apply a filter
      // });

      // // Add an event listener for mouse over
      // path.on("mouseover", function () {
      // //   console.log("Mouse over path object",path.id);
      //   // Do something here, such as change the fill color or apply a filter
      // });

      // Add the path object to the paths array
      paths.push(path);
    }
  });

  // Group the path objects into a single object
  // var pathsGroup = new fabric.Group(paths, {
  //   left: 10,
  //   top: 10,
  // });

  // Add the group object to the canvas and render it
  canvas.add.apply(canvas, paths);

  canvas.on("mouse:over", function (event) {
    event.target.set("fill", "red");
    canvas.renderAll();

    //   var target = canvas.findTarget(event.e);
    //   var point = new fabric.Point(event.e.clientX, event.e.clientY);

    //     // console.log(point);

    //     // paths.forEach(obj=>{
    //     //     if (obj.containsPoint(point)){
    //     //         console.log("object", obj.id);
    //     //         // if (obj.getPixelColor(point)){
    //     //         // console.log("object", obj.id,obj.getPixelColor(point));

    //     //         // }
    //     //     }
    //     // })
    //   //   paths.every((obj) => {
    //   //     if (obj.containsPoint(point)) {
    //   //       console.log("Clicked inside the path", obj.id);
    //   //       obj.set("fill", "red");
    //   //     } else {
    //   //         console.log("ok");
    //   //     }

    //   //     return false;
    //   //   });

    //   //   paths.forEach((obj) => {
    //   //     // Check if the point is inside the path object
    //   //     if (obj.containsPoint(point)) {
    //   //       console.log("Clicked inside the path", obj.id);
    //   //       obj.set("fill", "red");
    //   //     } else {
    //   //       obj.set("fill", "green");
    //   //     }
    //   //     canvas.renderAll();
    //   //   });
    //   // Check if an object was clicked
    //   if (target !== null) {
    //     // Set the active object to the clicked object
    //     canvas.setActiveObject(target);
    //     console.log(target.id);
    //   }

    //   paths.forEach(obj=>{
    //     if (obj.id==target.id){
    //         obj.set('fill','red');
    //     } else {
    //         obj.set('fill','green');
    //     }
    //   })
  });

  canvas.on("mouse:out", function (e) {
    e.target.set("fill", "green");
    canvas.renderAll();
  });

  // pathsGroup.selectable = false;
  // canvas.renderAll();
});